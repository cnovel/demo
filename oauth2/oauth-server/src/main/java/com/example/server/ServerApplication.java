package com.example.server;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**https://github.com/xuguofeng/springsecurityoauth2.git
 *
 * https://blog.csdn.net/ywdevil1314/article/details/107831837
 * 启动类
 *
 * @author novel
 * @date 2020/11/5
 */
@SpringBootApplication
@MapperScan("com.example.server.mapper")
public class ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

}
