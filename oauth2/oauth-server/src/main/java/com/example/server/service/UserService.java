package com.example.server.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 用户
 *
 * @author novel
 * @date 2020/11/5
 */
public interface UserService extends UserDetailsService {

}
