package com.example.server.mapper;

import com.example.server.domain.SysRole;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 角色
 *
 * @author novel
 * @date 2020/11/5
 */
public interface RoleMapper {
    @Select("select r.id,r.role_name roleName ,r.role_desc roleDesc " +
            "FROM sys_role r,sys_user_role ur " +
            "WHERE r.id=ur.rid AND ur.uid=#{uid}")
    List<SysRole> findByUid(Integer uid);
}
