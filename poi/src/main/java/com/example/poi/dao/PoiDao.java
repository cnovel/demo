package com.example.poi.dao;

import com.example.poi.model.Teacher;

import java.util.List;

/**
 * @author lz
 * @date 2019/12/31
 */
public interface PoiDao {
    public List<Teacher> getTeachers();
}
