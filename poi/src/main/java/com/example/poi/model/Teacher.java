package com.example.poi.model;

import lombok.Data;

/**
 * @author lz
 * @date 2019/12/31
 */
@Data
public class Teacher {
    private String id;
    private String name;
    private String pid;
    private String password;

    public Teacher(String id, String name, String pid, String password) {
        this.id = id;
        this.name = name;
        this.pid = pid;
        this.password = password;
    }
}
