package com.example.poi.controller;

import com.example.poi.service.PoiService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @author lz
 * @date 2019/12/31
 */
@RestController
public class TestController {

    private final PoiService poiService;

    public TestController(PoiService poiService) {
        this.poiService = poiService;
    }

    @RequestMapping(value = "/fileUpload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity fileUpload(@RequestParam("file") MultipartFile file) {
        return poiService.fileUpload(file);
    }

    @RequestMapping(value = "downLoadExcel", method = RequestMethod.GET)
    public void downLoadExcel(HttpServletResponse response) {
        poiService.downLoadExcel(response);
    }
}
