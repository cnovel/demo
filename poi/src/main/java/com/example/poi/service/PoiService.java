package com.example.poi.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @author lz
 * @date 2019/12/31
 */
public interface PoiService {
    /**
     * 上传/导入
     *
     * @param file
     * @return
     */
    ResponseEntity fileUpload(MultipartFile file);

    /**
     * 下载/导出
     *
     * @param response
     */
    void downLoadExcel(HttpServletResponse response);
}