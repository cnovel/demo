# demo

#### 项目介绍
springboot技术整合

#### 已完成的整合技术

1.  excel poi 的简单使用
2.  lambda 表达式的一些使用方式
3.  log 项目日志的记录
4.  MyBatis Plus 和 SpringBoot 的整合使用
5.  Redis 和 SpringBoot 的整合使用
6.  Shiro 和 SpringBoot 的整合使用
7.  Shiro、Redis 和 SpringBoot 的整合使用
8.  Shiro、Redis 和 SpringBoot 的整合使用（自定义session存在方式）
9.  Swagger 和 SpringBoot 的整合使用
10. MyBatis 和 SpringBoot 的整合使用
11. cache 和 SpringBoot 的整合使用
12. ehcache 和 SpringBoot 的整合使用
13. redis-cache 和 SpringBoot 的整合使用
14. fastjson 和 SpringBoot 的整合使用
15. easypoi 和 SpringBoot 的整合使用
16. oss 和 SpringBoot 的整合使用
17. validator 和 SpringBoot 的整合使用
18. kafka 和 SpringBoot 的整合使用
19. javacv使用
20. kaptcha 和 SpringBoot 的整合使用
21. readenv 读取spring配置以及java环境变量使用
22. Swagger 自定义隐藏实体属性
23. data-rest 和 SpringBoot 的整合使用
24. poi 和 SpringBoot 的整合使用
25. qrcode 和 SpringBoot 的整合使用
26. websocket 和 SpringBoot 的整合使用
27. rocketMQ 和 SpringBoot 的整合使用
28. mail 和 SpringBoot 的整合使用
29. actuator 和 SpringBoot 的整合使用
30. admin 和 SpringBoot 的整合使用
31. webflux 和 SpringBoot 的整合使用
32. security 和 SpringBoot 的整合使用
33. guava 使用
34. i18n 和 SpringBoot 的整合使用
35. java 9-16 一些新用法和功能
36. docker api 和 SpringBoot 的整合使用
