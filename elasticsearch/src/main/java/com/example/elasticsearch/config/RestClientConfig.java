package com.example.elasticsearch.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

/**
 * RestHighLevelClient 配置类
 *
 * @author novel
 * @date 2020/11/18
 */
//@Configuration
public class RestClientConfig extends AbstractElasticsearchConfiguration {

    @Override
//    @Bean
    public RestHighLevelClient elasticsearchClient() {

        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo("10.168.1.128:9200")
                .build();

        return RestClients.create(clientConfiguration).rest();
    }
}
