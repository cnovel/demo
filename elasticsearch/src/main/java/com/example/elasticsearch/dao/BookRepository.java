package com.example.elasticsearch.dao;

import com.example.elasticsearch.entity.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


/**
 * @author novel
 * @date 2020/11/18
 */
@Repository
public interface BookRepository extends ElasticsearchRepository<Book, String> {
}
