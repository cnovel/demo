package com.example.elasticsearch;

import com.alibaba.fastjson.JSONObject;
import com.example.elasticsearch.entity.Book;
import com.google.common.collect.Lists;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * @author novel
 * @date 2020/11/17
 */
public class ElasticsearchTest {
    TransportClient client;

    @BeforeEach
    public void before() throws UnknownHostException {
        client = new PreBuiltTransportClient(Settings.EMPTY);
        client.addTransportAddress(new TransportAddress(InetAddress.getByName("10.168.1.128"), 9300));
    }


    @AfterEach
    public void after() {
        client.close();
    }

    /**
     * 创建索引
     */
    @Test
    public void createIndex() {
        CreateIndexResponse indexResponse = client.admin().indices().prepareCreate("dangdang").get();
        System.out.println(indexResponse.isAcknowledged());
    }

    /**
     * 删除索引
     */
    @Test
    public void deleteIndex() {
        AcknowledgedResponse response = client.admin().indices().prepareDelete("dangdang").get();
        Assert.isTrue(response.isAcknowledged(), "删除失败");
    }

    /**
     * 创建索引 创建类型 创建mapping
     */
    @Test
    public void createIndexAndTypeMapping() throws ExecutionException, InterruptedException {
        CreateIndexRequest dangdang = new CreateIndexRequest("dangdang");
        dangdang.mapping("book", "{\"properties\":{\"id\":{\"type\":\"keyword\"},\"name\":{\"type\":\"keyword\"},\"price\":{\"type\":\"double\"},\"author\":{\"type\":\"keyword\"},\"des\":{\"type\":\"text\"},\"pubdate\":{\"type\":\"date\"}}}", XContentType.JSON);
        CreateIndexResponse indexResponse = client.admin().indices().create(dangdang).get();
        System.out.println(indexResponse.isAcknowledged());
    }

    /**
     * 创建文档
     */
    @Test
    public void createDocOptionId() {
        Book book = new Book();
        book.setId("1");
        book.setAge(23);
        book.setName("我们2020的故事");
        book.setBir(new Date());

        //添加一条文档
        IndexResponse response = client.prepareIndex("dangdang", "book", "1").setSource(JSONObject.toJSONString(book), XContentType.JSON).get();
        System.out.println(response.status());
    }

    /**
     * 创建文档
     */
    @Test
    public void createDocAutoId() {
        Book book = new Book();
        book.setId("2");
        book.setAge(23);
        book.setName("我们2020的故事");
        book.setBir(new Date());

        //添加一条文档
        IndexResponse response = client.prepareIndex("dangdang", "book").setSource(JSONObject.toJSONString(book), XContentType.JSON).get();
        System.out.println(response.status());
    }

    /**
     * 更新文档
     */
    @Test
    public void updateDoc() {
        Book book = new Book();
        book.setName("我们2020的故事222222");
        book.setBir(new Date());

        UpdateResponse response = client.prepareUpdate("dangdang", "book", "1").setDoc(JSONObject.toJSONString(book), XContentType.JSON).get();
        System.out.println(response.status());
    }

    /**
     * 更新文档
     */
    @Test
    public void deleteDoc() {

        DeleteResponse response = client.prepareDelete("dangdang", "book", "1").get();
        System.out.println(response.status());
    }

    /**
     * 查询一条文档
     */
    @Test
    public void findOneDoc() {

        GetResponse response = client.prepareGet("dangdang", "book", "zJeG1XUBWFHnTPMy01ZK").get();
        System.out.println(response.getSource());
    }

    /**
     * 查询所有文档
     */
    @Test
    public void findSearch() {
        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        SearchResponse response = client.prepareSearch("dangdang").setTypes("book").setQuery(matchAllQueryBuilder).get();

        System.out.println(response.getHits().getTotalHits());
        Arrays.stream(response.getHits().getHits()).map(SearchHit::getSourceAsMap).forEach(System.out::println);
    }

    /**
     * 查询  条件
     */
    @Test
    public void testQuery() {
        TermQueryBuilder matchAllQueryBuilder = QueryBuilders.termQuery("age", 23);
        SearchResponse response = client.prepareSearch("dangdang").setTypes("book").setQuery(matchAllQueryBuilder).get();

        System.out.println(response.getHits().getTotalHits());
        Arrays.stream(response.getHits().getHits()).map(SearchHit::getSourceAsMap).forEach(System.out::println);
    }

    /**
     * 复杂查询
     */
    @Test
    public void testQuery2() {
        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();

        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("age", 24);

        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("age").gte(22).lte(24);
        WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery("name", "我*");
        PrefixQueryBuilder prefixQueryBuilder = QueryBuilders.prefixQuery("name", "我");
        IdsQueryBuilder idsQueryBuilder = QueryBuilders.idsQuery().addIds("zJeG1XUBWFHnTPMy01ZK");
        //0-2 不允许模糊  3-5可以一个模糊 >5最多两个模糊
        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("name", "我们2020的故事");
        getResult(fuzzyQueryBuilder);
    }


    /**
     * 高亮查询
     */
    @Test
    public void testQuery3() {
        PrefixQueryBuilder prefixQueryBuilder = QueryBuilders.prefixQuery("name", "我");

        HighlightBuilder highlightBuilder = new HighlightBuilder();

        highlightBuilder.field("*");//所有字段高亮
        highlightBuilder.requireFieldMatch(false);//所有字段高亮
        highlightBuilder.preTags("<span style='color:red;'>").postTags("</span>");//设置高亮标签

        SearchResponse response = client.prepareSearch("dangdang").setTypes("book")
                .setQuery(prefixQueryBuilder)
                .highlighter(highlightBuilder)//高亮
                .get();

        System.out.println(response.getHits().getTotalHits());
        SearchHit[] hits = response.getHits().getHits();
        Arrays.stream(hits).forEach(System.out::println);
        List<Object> list = Lists.newArrayList();
        for (SearchHit hit : hits) {
            Map<String, HighlightField> highlightFieldMap = hit.getHighlightFields();
            Map<String, Object> source = hit.getSourceAsMap();
            Book book = JSONObject.parseObject(JSONObject.toJSONString(source), Book.class);
            if (highlightFieldMap.containsKey("name")) {
                HighlightField name = highlightFieldMap.get("name");
                book.setName(name.getFragments()[0].string());
            }


            list.add(book);
        }

        list.forEach(System.out::println);
    }

    /**
     * 过滤查询
     */
    @Test
    public void testQuery4() {
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("age").gte(0).lt(50);
        SearchResponse response = client.prepareSearch("dangdang").setTypes("book")
                .setPostFilter(rangeQueryBuilder)
                .setQuery(QueryBuilders.matchAllQuery())
                .get();
        Arrays.stream(response.getHits().getHits()).forEach(System.out::println);
    }

    private void getResult(QueryBuilder queryBuilder) {
        SearchResponse response = client.prepareSearch("dangdang").setTypes("book")
                .setQuery(queryBuilder)
                .setFrom(0)//设置偏移量
                .setSize(2)//设置页大小
                .addSort("age", SortOrder.DESC)//设置排序
                .get();

        System.out.println(response.getHits().getTotalHits());
        Arrays.stream(response.getHits().getHits()).map(SearchHit::getSourceAsMap).forEach(System.out::println);
    }
}
