package com.example.elasticsearch;

import com.example.elasticsearch.dao.BookRepository;
import com.example.elasticsearch.entity.Book;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@SpringBootTest
class ElasticsearchApplicationTests {
    @Autowired
    RestHighLevelClient highLevelClient;
    @Autowired
    BookRepository bookRepository;

    @Test
    void contextLoads() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest("ems", "emp", "1");
        DeleteResponse deleteResponse = highLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(deleteResponse.status());
    }

    @Test
    void contextLoads2() {
        Book book = new Book();
        book.setId("3");
        book.setName("java从入门到放弃");
        book.setAge(33);
        book.setBir(new Date());

        Book save = bookRepository.save(book);
        System.out.println(save);
    }

    @Test
    void contextLoads3() {
        bookRepository.deleteAll();
    }
    @Test
    void contextLoads4() {
        Optional<Book> bookOptional = bookRepository.findById("2");
        bookOptional.ifPresent(System.out::println);
    }

    @Test
    void contextLoads5() {
        Iterable<Book> books = bookRepository.findAll(Sort.by(Sort.Order.desc("age")));
        books.forEach(System.out::println);
    }
}
