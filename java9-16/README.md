

# 1.性能提升

**垃圾收集器:**

- `java8`默认的配置为`UseParallelGC`。虚拟机运行在`Server`模式下的默认值。使用`Parallel Scavenge + Serial Old`的收集器组合进行内存回收。有很多项目会使用`UseConeMarkSweepGC`。使用`ParNew + CMS + Serial Old`的收集器组合进行内存回收。`Serial Old`收集器将作为`CMS`收集器出现`Concurrent Mode Failure`失败后的后备收集器使用。`java14`中移除了`CMS`；

  ```bash
  //**查看GC配置
  java -XX:+PrintCommandLineFlags -version
  ```

- `java9`默认的配置为`UseG1GC`。使用`G1`收集器进行内存回收；
- `java10`改进`G1`收集器，允许并行`Full GC`，改善`G1`的延迟。`G1`会尽量避免`Full GC`，但是仍然会出现`Full GC`。`java10`使用并行的`标记-清除-压缩`算法，可用`-XX:ParallelGCThreads`配置线程数。
- `java11`引进了配置`UseEpsilonGC`。使用`Epsilon`垃圾收集器进行内存回收。它是一个无操作的垃圾收集器，处理内存分配但不实现任何实际内存回收机制，一旦可用堆内存用完，`JVM`就会退出；
- `java11`引进了配置`UseZGC`。使用`ZGC`垃圾收集器进行内存回收。它是一个可伸缩低延迟垃圾收集器，但还是实验性的，不建议用到生产环境；

# 2.语法特性

## java9

**集合工厂方法**

集合工厂方法(创建不可变的集合)，简化创建集合的代码，特别是Map；

```java
public class MyCollections {

    //集合工厂方法，简化创建集合的代码
    public static void test() {
        List list = List.of(1, 2, 3);
        System.out.println(list);

        Set set = Set.of(1, 2, 3);
        System.out.println(set);

        Map map = Map.of("key1", "value1", "key2", "value2");
        System.out.println(map);
    }
}
```

**CompletableFuture新增方法**

```java
public class MyCompletableFuture {

    public static void main(String[] args) throws Exception {
        //工厂方法，返回异常完成的CompletableFuture
        CompletableFuture<String> future1 = CompletableFuture.failedFuture(new RuntimeException("错误"));
        //future1.get();

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (Exception ignored) {}

            return "pings1";
        });
        //在给定的时间内，如果future2能完成就返回future2的返回值，否则返回默认值
        //CompletableFuture<String> future3 = future2.completeOnTimeout("pings2", 4, TimeUnit.SECONDS);
        //System.out.println(future3.get());

        //在给定的时间内，如果future2能完成就返回future2的返回值，否则以TimeoutException完成
        CompletableFuture<String> future4 = future2.orTimeout(4, TimeUnit.SECONDS);
        System.out.println(future4.get());
    }
}
```

**接口私有方法**

现在接口和抽象方法的区别：

- 抽象方法可以定义各种作用域的属性，接口只能定义公有的静态属性
- 抽象方法可以定义各种作用域的方法，接口只能定义私有方法和公用方法的默认实现

```java
public interface MyInterface {
    private void b() {
        //可使用this关键字
        System.out.println("b: " + this.getClass().getSimpleName());
    }

    class MyInterfaceImpl implements MyInterface { }

    static void main(String[] args) {
        MyInterface myInterface = new MyInterfaceImpl();
        myInterface.b();
    }
}
```

**Optional新增方法**

```java
public class MyOptional {

    public static void main(String[] args) {
        //Optional.stream
        List<Optional<String>> list = List.of(Optional.of("A"), Optional.empty(), Optional.of("B"));
        list.stream().flatMap(Optional::stream).forEach(System.out::println);

        //Optional.ifPresentOrElse
        List<Integer> list1 = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Optional<Integer> optional = list1.stream().filter(i -> i > 5).findAny();
        optional.ifPresentOrElse(System.out::println, () -> { throw new IllegalArgumentException(); });

        //Optional.or，与orElseGet区别：orElseGet参数返回Optional包装的类型，or仍然返回Optional
        optional = list1.stream().filter(i -> i > 10).findAny();
        Optional optional1 = optional.or(() -> Optional.of(100));
        int rst = optional.orElseGet(() -> 100);
        System.out.println(optional1.get());
        System.out.println(rst);
    }
}
```

**stream新增方法**

```java
public class MyStream {

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        //takeWhile，返回符合表达式的元素，直到第一次不满足表达式
        List<Integer> list1 = list.stream().takeWhile(i -> i < 5).collect(toList());
        System.out.println(list1);

        //dropWhile，删除表达式的元素，直到第一次不满足表达式
        List<Integer> list2 = list.stream().dropWhile(i -> i < 5 || i > 9).collect(toList());
        System.out.println(list2);

        //for(int i = 1; i < 10; i +=2)
        IntStream.iterate(1, x -> x < 10, x -> x + 2).forEach(System.out::println);
        Stream.iterate("ping", s -> s.length() < 10, s -> s = s + "1").forEach(System.out::println);

        //元素不支持是list，不知道实际有什么用
        Stream.ofNullable(null).forEach(System.out::println);
        //通过这种方法可以避免list3为空时出现空指针，还是感觉没啥用
        List list3 = null;
        List<Integer> list4 = List.of(1, 2);
        Stream.ofNullable(list3).flatMap(List::stream).forEach(System.out::println);
        Stream.ofNullable(list4).flatMap(List::stream).forEach(System.out::println);
    }
}
```

**try-with-resources改进**

```java
public class MyTryWithResources {

    public static void main(String[] args) throws IOException {
        Reader reader = new StringReader("pings");
        BufferedReader br = new BufferedReader(reader);

        //相对于jdk7的改进，可引用外部变量br，br必须是final
        try (reader; br) {
            System.out.println(br.readLine());
        }
    }
}
```

## java10

**集合copyOf静态方法**

```java
public class MyCollections {

    //集合工厂方法，简化创建集合的代码
    public static void test() {
        var list = List.of(1, 2, 3);
        var newList = List.copyOf(list);
        System.out.println(newList);

        var map = Map.of("key1", "value1", "key2", "value2");
        System.out.println(Map.copyOf(map));
    }

    public static void main(String[] args) {
        test();
    }
}
```

**局部变量类型推断**

引入var作为局部变量类型推断标识符，仅适用于局部变量，不能使用于方法形式参数，构造函数形式参数，方法返回类型，catch形式参数或任何其他类型的变量声明；

```java
public class MyVar {

    public static void main(String[] args) {
        var ss = 123;

        //可以推断出泛型的类型
        var list = List.of(1, 2, 3, 4);
        var newList = list.stream().map(Integer::getClass).collect(toList());
        System.out.println(newList);
    }
}
```

## java11

**字符串新增方法**

```java
public class MyString {

    public static void main(String[] args) {
        String str = "pings";

        boolean isBlank = str.isBlank();  //判断字符串是空白
        System.out.println(isBlank);
        //str = null;
        //System.out.println(str.isBlank()); //只能判断非null，我还以为可以替换StringUtils.isBlank()方法了

        boolean isEmpty = str.isEmpty();  //判断字符串是否为空
        System.out.println(isEmpty);

        String result1 = str.strip();    //首位空白
        String result2 = str.stripTrailing();  //去除尾部空白
        String result3 = str.stripLeading();  //去除首部空白

        System.out.println("12".repeat(5));  //复制几遍字符串
        str.lines().forEach(System.out::println);  //按行读取
    }
}
```

**局部变量类型推断增强**

```java
public class MyVar {

    public static void main(String[] args) {
        //var可引用到lambda表达式的参数上，本来lambda表达式的参数不用写类型也可以，那这样写有什么作用了
        Consumer<String> consumer1 = (var t) -> System.out.println(t.toUpperCase());
        consumer1.accept("pings");

        //lambda表达式的参数如果要加注解，就必须有类型
        //作用就是要在lambda表达式的参数加注解时定义参数类型
        Consumer<String> consumer2 = (@Deprecated var t) -> System.out.println(t.toUpperCase());
        consumer2.accept("pings");
    }
}
```

**HttpClient**

```java
public class MyHttpClient {

    public static void test() throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create("http://www.baidu.com")).build();
        HttpResponse.BodyHandler<String> handler = HttpResponse.BodyHandlers.ofString();

        //*同步调用
        //HttpResponse<String> response = client.send(request, handler);
        //String body = response.body();

        //**异步调用
        CompletableFuture<HttpResponse<String>> response = client.sendAsync(request, handler);
        HttpResponse<String> result = response.get();
        String body = result.body();

        System.out.println(body);
    }
}
```

**javac + java 命令一把梭**

以前编译一个 java 文件时，需要先 javac 编译为 class，然后再用 java 执行，现在可以一把梭了：

```bash
$ java HelloWorld.java
Hello Java 11!
```

**Java Flight Recorder 登陆 OpenJDK**
`Java Flight Recorder `是个灰常好用的调试诊断工具，不过之前是在 `Oracle JDK` 中，也跟着` JDK 11`开源了，`OpenJDK` 这下也可以用这个功能，真香！

## java12

**更简洁的 switch 语法**

在之前的 JAVA 版本中，switch 语法还是比较啰嗦的，如果多个值走一个逻辑需要写多个 case：

```java
DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
String typeOfDay = "";
switch (dayOfWeek) {
    case MONDAY:
    case TUESDAY:
    case WEDNESDAY:
    case THURSDAY:
    case FRIDAY:
        typeOfDay = "Working Day";
        break;
    case SATURDAY:
    case SUNDAY:
        typeOfDay = "Day Off";
}
```

到了 JAVA 12，这个事情就变得很简单了，几行搞定，而且！还支持返回值：

```java
typeOfDay = switch (dayOfWeek) {
    case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> "Working Day";
    case SATURDAY, SUNDAY -> "Day Off";
};
```

**instanceof + 类型强转一步到位**

之前处理动态类型碰上要强转时，需要先 instanceof 判断一下，然后再强转为该类型处理：

```java
Object obj = "Hello Java 12!";
if (obj instanceof String) {
    String s = (String) obj;
    int length = s.length();
}
```

现在 instanceof 支持直接类型转换了，不需要再来一次额外的强转：

```java
Object obj = "Hello Java 12!";
if (obj instanceof String str) {
    int length = str.length();
}
```

## java13

**switch 语法再增强**

JAVA 12 中虽然增强了 swtich 语法，但并不能在 -> 之后写复杂的逻辑，JAVA 12 带来了 swtich更完美的体验，就像 lambda 一样，可以写逻辑，然后再返回：

```java
typeOfDay = switch (dayOfWeek) {
    case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> {
        // do sth...
    	yield "Working Day";
    }
    case SATURDAY, SUNDAY -> "Day Off";
};
```

**文本块（Text Block）的支持**

你是否还在为大段带换行符的字符串报文所困扰，换行吧一堆换行符，不换行吧看着又难受：

```java
String json = "{\"id\":\"1697301681936888\",\"nickname\":\"空无\",\"homepage\":\"https://juejin.cn/user/1697301681936888\"}";
```

JAVA 13 中帮你解决了这个恶心的问题，增加了文本块的支持，现在可以开心的换行拼字符串了，就像用模板一样：

```java
String json = """
  {
      "id":"1697301681936888",
      "nickname":"空无",
      "homepage":"https://juejin.cn/user/1697301681936888"
  }
  """;
```

## java14

新增的 record 类型，干掉复杂的 POJO 类

一般我们创建一个 POJO 类，需要定义属性列表，构造函数，getter/setter，比较麻烦。JAVA 14 为我们带来了一个便捷的创建类的方式 - `record`

```java
public record UserDTO(String id,String nickname,String homepage) { };

public static void main( String[] args ){
	UserDTO user = new UserDTO("1697301681936888","空无","https://juejin.cn/user/1697301681936888");
    System.out.println(user.id);
    System.out.println(user.nickname);
    System.out.println(user.id);
}
```

IDEA 也早已支持了这个功能，创建类的时候直接就可以选： 

![](https://i.loli.net/2021/05/27/xYRnq9ESBymdjVO.png)

不过这个只是一个语法糖，编译后还是一个 Class，和普通的 Class 区别不大

**更直观的 NullPointerException 提示**

NullPointerException 算是 JAVA 里最常见的一个异常了，但这玩意提示实在不友好，遇到一些长一点的链式表达式时，没办法分辨到底是哪个对象为空。 

比如下面这个例子中，到底是 innerMap 为空呢，还是 effected 为空呢？

```java
Map<String,Map<String,Boolean>> wrapMap = new HashMap<>();
wrapMap.put("innerMap",new HashMap<>());

boolean effected = wrapMap.get("innerMap").get("effected");

// StackTrace:
Exception in thread "main" java.lang.NullPointerException
	at org.example.App.main(App.java:50)
```

JAVA 14 也 get 到了 JAVAER 们的痛点，优化了 NullPointerException 的提示，让你不在困惑，一眼就能定位到底“空”在哪！

```java
Exception in thread "main" java.lang.NullPointerException: Cannot invoke "java.lang.Boolean.booleanValue()" because the return value of "java.util.Map.get(Object)" is null
	at org.example.App.main(App.java:50)
```

现在的 StackTrace 就很直观了，直接告诉你 effected 变量为空，再也不用困惑！

**安全的堆外内存读写接口，别再玩 Unsafe 的骚操作了**

在之前的版本中，JAVA 如果想操作堆外内存（DirectBuffer），还得 Unsafe 各种 copy/get/offset。现在直接增加了一套安全的堆外内存访问接口，可以轻松的访问堆外内存，再也不用搞 Unsafe 的骚操作了。

```java
// 分配 200B 堆外内存
MemorySegment memorySegment = MemorySegment.allocateNative(200);

// 用 ByteBuffer 分配，然后包装为 MemorySegment
MemorySegment memorySegment = MemorySegment.ofByteBuffer(ByteBuffer.allocateDirect(200));

// MMAP 当然也可以
MemorySegment memorySegment = MemorySegment.mapFromPath(
  Path.of("/tmp/memory.txt"), 200, FileChannel.MapMode.READ_WRITE);

// 获取堆外内存地址
MemoryAddress address = MemorySegment.allocateNative(100).baseAddress();

// 组合拳，堆外分配，堆外赋值
long value = 10;
MemoryAddress memoryAddress = MemorySegment.allocateNative(8).baseAddress();
// 获取句柄
VarHandle varHandle = MemoryHandles.varHandle(long.class, ByteOrder.nativeOrder());
varHandle.set(memoryAddress, value);

// 释放就这么简单，想想 DirectByteBuffer 的释放……多奇怪
memorySegment.close();
```

**新增的 jpackage 打包工具，直接打包二进制程序，再也不用装 JRE 了**

之前如果想构建一个可执行的程序，还需要借助三方工具，将 JRE 一起打包，或者让客户电脑也装一个 JRE 才可以运行我们的 JAVA 程序。 

现在 JAVA 直接内置了 jpackage 打包工具，帮助你一键打包二进制程序包，终于不用乱折腾了

## java15

**ZGC 和 Shenandoah 两款垃圾回收器正式登陆**

在 JAVA 15中，ZGC 和 Shenandoah 再也不是实验功能，正式登陆了（不过 G1 仍然是默认的）。如果你升级到 JAVA 15 以后的版本，就赶快试试吧，性能更强，延迟更低

**封闭（Sealed ）类**

JAVA 的继承目前只能选择允许继承和不允许继承（final 修饰），现在新增了一个封闭（Sealed ）类的特性，可以指定某些类才可以继承：

```java
public sealed interface Service permits Car, Truck {

    int getMaxServiceIntervalInMonths();

    default int getMaxDistanceBetweenServicesInKilometers() {
        return 100000;
    }

}
```

