package com.demo.grammar.java9;

/**
 * 接口私有方法
 *
 * @author novel
 * @date 2021/5/26 14:36
 */
public interface MyInterface {
    private void b() {
        //可使用this关键字
        System.out.println("b: " + this.getClass().getSimpleName());
    }

    class MyInterfaceImpl implements MyInterface {
    }

    static void main(String[] args) {
        MyInterface myInterface = new MyInterfaceImpl();
        myInterface.b();
    }
}