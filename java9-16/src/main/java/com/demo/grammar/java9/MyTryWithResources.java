package com.demo.grammar.java9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * try-with-resources改进
 *
 * @author novel
 * @date 2021/5/26 14:41
 */
public class MyTryWithResources {

    public static void main(String[] args) {
        Reader reader = new StringReader("pings");
        BufferedReader br = new BufferedReader(reader);

        //相对于jdk7的改进，可引用外部变量br，br必须是final
        try (reader; br) {
            System.out.println(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}