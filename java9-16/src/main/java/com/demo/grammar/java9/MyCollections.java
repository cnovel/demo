package com.demo.grammar.java9;


import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * java9
 * 集合工厂方法
 *
 * @author novel
 * @date 2021/5/26 13:58
 */
public class MyCollections {

    /**
     * 集合工厂方法，简化创建集合的代码
     */
    @Test
    public void test() {
        List<Integer> list = List.of(1, 2, 3);
        System.out.println(list);

        Set<Integer> set = Set.of(1, 2, 3);
        System.out.println(set);

        Map<String, String> map = Map.of("key1", "value1", "key2", "value2");
        System.out.println(map);
    }
}