package com.demo.grammar.java10;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 局部变量类型推断
 *
 * @author novel
 * @date 2021/5/26 14:46
 */
public class MyVar {

    public static void main(String[] args) {
        var ss = 123;

        //可以推断出泛型的类型
        var list = List.of(1, 2, 3, 4);
        var newList = list.stream().map(Integer::getClass).collect(Collectors.toList());
        System.out.println(newList);
    }
}