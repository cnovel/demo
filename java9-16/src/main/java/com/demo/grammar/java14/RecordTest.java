package com.demo.grammar.java14;

/**
 * @author novel
 * @date 2021/5/27 14:56
 */
public class RecordTest {
    public record UserDTO(String id, String nickname, String homepage) {
    }

    public static void main(String[] args) {
        UserDTO user = new UserDTO("1697301681936888", "空无", "https://juejin.cn/user/1697301681936888");
        System.out.println(user.id);
        System.out.println(user.nickname);
        System.out.println(user.id);
    }
}
