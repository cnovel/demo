package com.demo.grammar.java14;

import java.util.HashMap;
import java.util.Map;

/**
 * @author novel
 * @date 2021/5/27 15:03
 */
public class NullPointerExceptionTest {
    public static void main(String[] args) {
        Map<String, Map<String, Boolean>> wrapMap = new HashMap<>();
        wrapMap.put("innerMap", new HashMap<>());

        boolean effected = wrapMap.get("innerMap").get("effected");

// StackTrace:        Exception in thread "main" java.lang.NullPointerException        at org.example.App.main(App.java:50)
    }
}
