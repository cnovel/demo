package com.demo.grammar.java13;

import org.junit.jupiter.api.Test;

/**
 * @author novel
 * @date 2021/5/27 14:52
 */
public class TextTest {
    @Test
    void test1() {
        String json = "{\"id\":\"1697301681936888\",\"nickname\":\"空无\",\"homepage\":\"https://juejin.cn/user/1697301681936888\"}";
        System.out.println(json);
        String json2 = """
                {
                    "id":"1697301681936888",
                    "nickname":"空无",
                    "homepage":"https://juejin.cn/user/1697301681936888"
                }
                """;
        System.out.println(json2);
    }
}
