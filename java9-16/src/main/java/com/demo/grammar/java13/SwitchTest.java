package com.demo.grammar.java13;

import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * @author novel
 * @date 2021/5/27 14:51
 */
public class SwitchTest {
    @Test
    void test1() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        String typeOfDay = "";
        typeOfDay = switch (dayOfWeek) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> {
                // do sth...
                yield "Working Day";
            }
            case SATURDAY, SUNDAY -> "Day Off";
        };
        System.out.println(typeOfDay);
    }
}
