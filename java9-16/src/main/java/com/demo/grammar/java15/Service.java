package com.demo.grammar.java15;

/**
 * 指定接口的实现类
 *
 * @author 李振
 * @since 2023/5/23 15:33
 */
public sealed interface Service permits Service.Car, Service.Truck {

    int getMaxServiceIntervalInMonths();

    default int getMaxDistanceBetweenServicesInKilometers() {
        return 100000;
    }

    final class Car implements Service {
        @Override
        public int getMaxServiceIntervalInMonths() {
            return 0;
        }
    }

    final class Truck implements Service {
        @Override
        public int getMaxServiceIntervalInMonths() {
            return 0;
        }
    }
}