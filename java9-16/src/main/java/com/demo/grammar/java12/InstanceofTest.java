package com.demo.grammar.java12;

import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * @author novel
 * @date 2021/5/27 14:47
 */
public class InstanceofTest {
    @Test
    void test1() {
        Object obj = "Hello Java 12!";
        if (obj instanceof String) {
            String s = (String) obj;
            int length = s.length();
            System.out.println(length);
        }
    }

    @Test
    void test2() {
        Object obj = "Hello Java 12!";
        if (obj instanceof String str) {
            int length = str.length();
            System.out.println(length);
        }
    }
}
