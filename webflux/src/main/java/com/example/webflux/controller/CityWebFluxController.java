package com.example.webflux.controller;

import com.example.webflux.domain.City;
import com.example.webflux.handler.CityHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

/**
 * @author novel
 * @date 2020/10/28
 */
@Controller
@RequestMapping(value = "/city")
public class CityWebFluxController {
    private static final String CITY_LIST_PATH_NAME = "cityList";
    private static final String CITY_PATH_NAME = "city";
    private static final String CITY_FORM_PATH_NAME = "cityForm";
    private static final String REDIRECT_TO_CITY_URL = "redirect:/city";

    private final CityHandler cityHandler;

    public CityWebFluxController(CityHandler cityHandler) {
        this.cityHandler = cityHandler;
    }
//
//    @ResponseBody
//    @GetMapping(value = "/{id}")
//    public Mono<City> findCityById(@PathVariable("id") Long id) {
//        return cityHandler.findCityById(id);
//    }
//
//    @ResponseBody
//    @GetMapping()
//    public Flux<City> findAllCity() {
//        return cityHandler.findAllCity();
//    }
//
//    @ResponseBody
//    @PostMapping()
//    public Mono<City> saveCity(@RequestBody City city) {
//        return cityHandler.save(city);
//    }
//
//    @ResponseBody
//    @PutMapping()
//    public Mono<City> modifyCity(@RequestBody City city) {
//        return cityHandler.modifyCity(city);
//    }
//
//    @ResponseBody
//    @DeleteMapping(value = "/{id}")
//    public Mono<Long> deleteCity(@PathVariable("id") Long id) {
//        return cityHandler.deleteCity(id);
//    }
//
//    @GetMapping("/hello")
//    public Mono<String> hello(final Model model) {
//        model.addAttribute("name", "泥瓦匠");
//        model.addAttribute("city", "浙江温岭");
//
//        String path = "hello";
//        return Mono.create(monoSink -> monoSink.success(path));
//    }

    //
//    @GetMapping("/page/list")
//    public String listPage(final Model model) {
//        final Flux<City> cityFluxList = cityHandler.findAllCity();
//        model.addAttribute("cityList", cityFluxList);
//        return CITY_LIST_PATH_NAME;
//    }
//
//    @GetMapping("/getByName")
//    public String getByCityName(final Model model,
//                                @RequestParam("cityName") String cityName) {
//        final Mono<City> city = cityHandler.getByCityName(cityName);
//        model.addAttribute("city", city);
//        return CITY_PATH_NAME;
//    }
    @GetMapping
    public String getCityList(final Model model) {
        model.addAttribute("cityList", cityHandler.findAllCity());
        return CITY_LIST_PATH_NAME;
    }

    @GetMapping("/create")
    public String createCityForm(final Model model) {
        model.addAttribute("city", new City());
        model.addAttribute("action", "create");
        return CITY_FORM_PATH_NAME;
    }

    @PostMapping("/create")
    public String postCity(@ModelAttribute City city, Model model) {
        model.addAttribute("res", cityHandler.save(city));
        return REDIRECT_TO_CITY_URL;
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String getCity(@PathVariable Long id, final Model model) {
        final Mono<City> city = cityHandler.findCityById(id);
        model.addAttribute("city", city);
        model.addAttribute("action", "update");
        return CITY_FORM_PATH_NAME;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String putBook(@ModelAttribute City city, Model model) {
        model.addAttribute("res", cityHandler.modifyCity(city));
        return REDIRECT_TO_CITY_URL;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteCityById(@PathVariable Long id, Model model) {
        model.addAttribute("res", cityHandler.deleteCity(id));
        return REDIRECT_TO_CITY_URL;
    }
}
