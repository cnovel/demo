package com.example.webflux.dao;

import com.example.webflux.domain.City;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * 城市
 *
 * @author novel
 * @date 2020/10/28
 */
@Repository
public interface CityRepository extends ReactiveMongoRepository<City, Long> {
    /**
     * 根据城市名称查询
     *
     * @param cityName 城市名称
     * @return 结果
     */
    Mono<City> findByCityName(String cityName);
}
