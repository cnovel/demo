package com.example.demo.dto;

import lombok.Data;

/**
 * @author novel
 * @date 2021/5/18 15:51
 */
@Data
public class ResultBean {
    public static final ResultBean FAIL = new ResultBean(500, "");
    private Integer code;
    private String msg;

    public ResultBean(String msg) {
        this(200, msg);
    }

    public ResultBean(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultBean() {
        this("");
    }
}
