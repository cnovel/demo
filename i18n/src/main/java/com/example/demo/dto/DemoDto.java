package com.example.demo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
public class DemoDto {

    @NotEmpty(message = "名称不能为空")
    private String name;
    @NotEmpty(message = "{demo.key.null}")
    @Length(min = 5, max = 25, message = "{demo.key.length}")
    private String key;

    @Pattern(regexp = "[012]", message = "无效的状态标志")
    private String state;
}