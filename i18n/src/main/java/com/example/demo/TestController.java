package com.example.demo;

import com.example.demo.dto.DemoDto;
import com.example.demo.dto.ResultBean;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Locale;

/**
 * 测试接口
 *
 * @author novel
 * @date 2021/5/17 17:44
 */
@RestController
public class TestController {
    private final MessageSource messageSource;

    public TestController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping("/test")
    public String testInfo() {
        return messageSource.getMessage("login.remember", null, LocaleContextHolder.getLocale());
    }

    @RequestMapping("test2")
    public ResultBean test(@Valid @RequestBody DemoDto dto) {
        System.out.println("test....................");
        return new ResultBean("test.........................");
    }
    @RequestMapping("getMessageByKey")
    public ResultBean getMessageByKey(@Valid @RequestBody DemoDto dto){
        String key = dto.getKey();
        String [] param = {"2019-8-8", "2019-9-9"};
        return new ResultBean(messageSource.getMessage(key, param, Locale.CHINA));
    }
}
