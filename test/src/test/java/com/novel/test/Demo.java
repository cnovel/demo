package com.novel.test;

/**
 * @author 李振
 * @since 2023/2/21 13:41
 */
public class Demo {
    public long fact(long n) {
        long r = 1;
        for (int i = 1; i < n; i++) {
            r = r * i;
        }
        return r;
    }
}
