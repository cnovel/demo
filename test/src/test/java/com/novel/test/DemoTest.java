package com.novel.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author 李振
 * @since 2023/2/21 13:43
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DemoTest {

    private List<User> userList;

    @Test
    void fact() {
        Demo demo = new Demo();
        assertEquals(362880, demo.fact(10));
    }

    @BeforeAll
    void before() {
        userList = new ArrayList<>();
        userList.add(new User("张三", 32));
        userList.add(new User("李四", 41));
        userList.add(new User("王五", 30));
    }

    @Test
    void sort() {
        val users = userList.stream().sorted(Comparator.comparingInt(User::getAge)).toList();
        assertEquals("王五",users.get(0).getUserName());
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class User {
    private String userName;
    private Integer age;
}