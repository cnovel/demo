package org.example;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 李振
 * @since 2023/5/26 15:47
 */
public class Demo5 {
    public static void main(String[] args) {
        System.out.println(new Demo5().containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3));
    }

    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i]) && i - map.get(nums[i]) <= k) {
                return true;
            }
            map.put(nums[i], i);
        }
        return false;
    }
}
