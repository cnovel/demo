package com.example.client.service;

import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class TopicOrderService {

    private final RabbitTemplate rabbitTemplate;


    //模拟用户下单
    public void makeOrder(String userid, String productid, int num) {
        //1.根据商品id查询库存是否足够
        //2.保存订单
        String orderId = UUID.randomUUID().toString();
        System.out.println("订单生产成功：" + orderId);
        //3.通过MQ来完成消息的分发
        //参数1：交换机 参数2：路由key/queue队列名称 参数3：消息内容
        String exchangeName = "topic_order_exchange";

        String routingKey = "com.email";
        rabbitTemplate.convertAndSend(exchangeName, routingKey, orderId);
    }

}