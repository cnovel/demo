package com.example.client;

import com.example.client.service.TopicOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ClientApplicationTests {
    @Autowired
    private TopicOrderService topicOrderService;

    @Test
    void contextLoads() {
        topicOrderService.makeOrder("1", "1", 12);
    }

}
