package com.example.server.service;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(bindings = @QueueBinding(
        value = @Queue(value = "email.topic.queue", durable = "true", autoDelete = "false"),
        exchange = @Exchange(value = "topic_order_exchange", type = ExchangeTypes.TOPIC),
        key = "*.email.#"
))

/**
 * 短信服务
 */
public class TopicEmailConsumer {

    @RabbitHandler
    public void topicreviceMessage(String message) {
        System.out.println("email topic 接收到了的订单信息是：" + message);
    }
}
 