package com.example.server.service;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

@Component
//注解的方式  配置 交换器  和队列
@RabbitListener(bindings = @QueueBinding(
        value = @Queue(value = "duanxin.topic.queue", durable = "true", autoDelete = "false"),
        exchange = @Exchange(value = "topic_order_exchange", type = ExchangeTypes.TOPIC),
        key = "#.duanxin.#"

))

public class TopicDuanxinConsumer {

    @RabbitHandler
    public void topicreviceMessage(String message) {
        System.out.println("短信duanxin topic 接收到了的订单信息是：" + message);
    }
}