package com.example.unit;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = UnitTestApplication.class)
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceTest1 {


    @BeforeClass
    public static void beforeClass() {
        System.out.println("这是@BeforeClass方法");
    }

    @Before
    public void before() throws Exception {
        System.out.println("这是@Before方法");
    }

    @Test
    public void test1InsertUser() {
        System.out.println("Test");
    }


    @After
    public void after() throws Exception {
        System.out.println("这是@After方法");
    }

    @AfterClass
    public static void afterClass() throws Exception {
        System.out.println("这是@AfterClass方法");
    }
}
