package com.example.unit;

import com.example.unit.test.TestDemo;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(classes = UnitTestApplication.class)
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UnitTestApplicationTests {
    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    @Test
    public void get() {
        String ok = new TestDemo().get();
        Assert.assertEquals("get 返回错误", ok, "ok");
    }


    @BeforeClass
    public static void beforeClass() {
        System.out.println("这是@BeforeClass方法");
    }

    @Before
    public void before() throws Exception {
        System.out.println("这是@Before方法");
        mvc = MockMvcBuilders.webAppContextSetup(context).build();// 使用WebApplicationContext初始化mockMVC
    }

    @Test
    public void list() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/test").accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        int status = mvcResult.getResponse().getStatus();
        //打印出状态码，200就是成功
        System.out.println("状态码=" + status);
        System.out.println("结果=" + mvcResult.getResponse().getContentAsString());
        Assert.assertEquals(200, status);
    }

    @After
    public void after() throws Exception {
        System.out.println("这是@After方法");
    }

    @AfterClass
    public static void afterClass() throws Exception {
        System.out.println("这是@AfterClass方法");
    }
}
