package com.example.unit.controller;

import com.example.unit.model.TestModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试控制器
 *
 * @author novel
 * @date 2021/6/23 19:02
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping
    public List<TestModel> list() {
        List<TestModel> list = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            TestModel model = new TestModel();
            model.setId(i);
            model.setName("王五" + i);
            list.add(model);
        }
        return list;
    }

    @GetMapping("/1")
    public String get() {
        return "1";
    }
}
