package com.example.unit.model;

import lombok.Data;

/**
 * 测试模型
 *
 * @author novel
 * @date 2021/6/23 19:03
 */
@Data
public class TestModel {
    private Integer id;
    private String name;
}
