package com.example.qrcode.controller;

import com.example.qrcode.utils.QrCodeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * @author novel
 * @date 2020/8/24
 */
@RestController
public class QrCodeController {
    /**
     * 二维码
     *
     * @param request
     * @param response
     */
    @RequestMapping("/qrcode")
    public void qrcode(HttpServletRequest request, HttpServletResponse response) {
        StringBuffer url = request.getRequestURL();
        // 域名
        String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();

        // 再加上请求链接
        String requestUrl = tempContextUrl + "index.html";
        try {
            OutputStream os = response.getOutputStream();
            QrCodeUtils.encode(requestUrl, os);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 二维码
     * @param request
     * @param response
     */
    @RequestMapping("/qrcode2")
    public void qrcode2( HttpServletRequest request, HttpServletResponse response) {
        StringBuffer url = request.getRequestURL();
        // 域名
        String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(), url.length()).append("/").toString();

        // 再加上请求链接
        String requestUrl = tempContextUrl + "index.html";
        try {
            OutputStream os = response.getOutputStream();
            QrCodeUtils.encode(requestUrl, "/static/images/logo.jpg", os, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
