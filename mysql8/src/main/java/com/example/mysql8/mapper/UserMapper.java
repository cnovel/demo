package com.example.mysql8.mapper;

import com.example.mysql8.domain.User;

import java.util.List;

/**
 * @author lz
 * @date 2019/6/21
 */
public interface UserMapper {
    List<User> selectAll();
}
