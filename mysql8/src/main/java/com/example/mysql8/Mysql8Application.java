package com.example.mysql8;

import com.example.mysql8.domain.User;
import com.example.mysql8.mapper.UserMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
@MapperScan("com.example.mysql8.mapper")
public class Mysql8Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(Mysql8Application.class, args);
        UserMapper bean = run.getBean(UserMapper.class);
        List<User> users = bean.selectAll();
        System.out.println(users);
    }

}
