package com.example.security.dao;

import com.example.security.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 用户
 *
 * @author novel
 * @date 2020/10/29
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    /**
     * 查询 userEntity
     *
     * @param userName 用户名
     * @return 用户
     */
    UserEntity findFirstByUserName(String userName);
}
