package com.example.security.dao;

import com.example.security.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 角色
 *
 * @author novel
 * @date 2020/10/29
 */
@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {
}
