package com.example.security.service;

import com.example.security.entity.RoleEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * 角色
 *
 * @author novel
 * @date 2020/11/4
 */
public interface RoleService {
    List<RoleEntity> list();

    RoleEntity save(RoleEntity role);

    Optional<RoleEntity> findById(int id);

    void deleteById(int id);

    Page<RoleEntity> findAll(Example<RoleEntity> example, Pageable pageable);
}
