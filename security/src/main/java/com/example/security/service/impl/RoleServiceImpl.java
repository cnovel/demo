package com.example.security.service.impl;

import com.example.security.dao.RoleRepository;
import com.example.security.entity.RoleEntity;
import com.example.security.service.RoleService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * 角色
 *
 * @author novel
 * @date 2020/11/4
 */
@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<RoleEntity> list() {
        return roleRepository.findAll();
    }

    @Override
    public RoleEntity save(RoleEntity role) {
        return roleRepository.save(role);
    }

    @Override
    public Optional<RoleEntity> findById(int id) {
        return roleRepository.findById(id);
    }

    @Override
    public void deleteById(int id) {
        roleRepository.deleteById(id);
    }

    @Override
    public Page<RoleEntity> findAll(Example<RoleEntity> example, Pageable pageable) {
        return  roleRepository.findAll(example, pageable);
    }
}
