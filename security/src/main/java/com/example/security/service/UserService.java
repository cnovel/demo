package com.example.security.service;

import com.example.security.entity.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * 用户
 *
 * @author novel
 * @date 2020/11/4
 */
public interface UserService extends UserDetailsService {
    List<UserEntity> list();

    Boolean add(UserEntity userEntity);

    Boolean deleteUserById(Integer id);

    UserEntity getUserById(Integer id);
}
