package com.example.security.common;

import java.io.Serializable;

/**
 * @author novel
 * @date 2020/10/29
 */
public class ResultInfo implements Serializable {

    private int code;

    private String message;

    private Object data;

    public ResultInfo() {
    }

    public ResultInfo(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ResultInfo success(String msg, Object data) {
        return new ResultInfo(0, msg, data);
    }

    public static ResultInfo error(int code, String msg) {
        return new ResultInfo(code, msg, null);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
