package com.example.security;

import com.example.security.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
class SecurityApplicationTests {
    @Autowired
    UserServiceImpl userService;

    @Transactional
    @Rollback(value = false)
    @Test
    void contextLoads() {
        userService.create("admin", "123456");
    }

}
