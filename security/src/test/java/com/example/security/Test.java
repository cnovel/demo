package com.example.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author novel
 * @date 2020/10/29
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("123456"));
    }
}
