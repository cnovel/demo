package com.example.mail.controller;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 邮箱
 *
 * @author novel
 * @date 2020/10/28
 */
@RestController
public class EmailController {

    private final JavaMailSender mailSender;

    public EmailController(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @GetMapping("send")
    public void send() {

        SimpleMailMessage message = new SimpleMailMessage();
        // 发件人
        message.setFrom("x@qq.com");
        // 收件人
        message.setTo("xxx@163.com");
        // 邮件标题
        message.setSubject("Java发送邮件第二弹");
        // 邮件内容
        message.setText("你好，这是一条用于测试Spring Boot邮件发送功能的邮件！哈哈哈~~~");
        // 抄送人
        message.setCc("xxx.com");
        mailSender.send(message);
    }
}
