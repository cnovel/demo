package com.example.mybatisplus;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * 代码生成器
 *
 * @author novel
 * @date 2021/11/24 20:28
 */
public class MybatisPlusGenerator {
    public static void main(String[] args) {
        FastAutoGenerator.create(
                        "jdbc:mysql://127.0.0.1:3306/test2?useUnicode=true&characterEncoding=utf8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&useSSL=false&autoReconnect=true&allowPublicKeyRetrieval=true",
                        "root",
                        "123456")
                .globalConfig(builder -> {
                    builder
                            .disableOpenDir()
                            .author("李振") // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .dateType(DateType.ONLY_DATE)
                            .outputDir("./src/main/java/"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder
                            .entity("domain") // Entity 包名 默认值:entity
                            .parent("com.novel") // 设置父包名
//                            .moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "./src/main/resources/mapper/")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    // 设置需要生成的表名
                    builder.addInclude("invoice_data", "user_status")
//                            .addTablePrefix("t_", "c_")// 设置过滤表前缀
                            .controllerBuilder().enableRestStyle().enableHyphenStyle()
                            .entityBuilder().enableLombok()
                    ;
                })
//                .strategyConfig(builder -> {
//                    builder.entityBuilder()
//                            // 添加表字段填充
//                            .addTableFills(new Column("create_time", FieldFill.INSERT))
//                            // 添加表字段填充
//                            .addTableFills(new Property("update_time", FieldFill.INSERT_UPDATE))
//                            // 设置id策略微雪花id
//                            .idType(IdType.ASSIGN_ID);
//                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
