package com.example.docker;

import com.example.docker.service.DockerClientService;
import com.github.dockerjava.api.async.ResultCallbackTemplate;
import com.github.dockerjava.api.model.Statistics;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@Slf4j
class DockerApiApplicationTests {
    @Autowired
    DockerClientService dockerClientService;

    @SneakyThrows
    @Test
    void contextLoads() {
        CountDownLatch downLatch = new CountDownLatch(3);
        dockerClientService.statsContainer("redis", new StatsCallback(downLatch));
        downLatch.await(5, TimeUnit.SECONDS);
    }

    public static class StatsCallback extends ResultCallbackTemplate<StatsCallback, Statistics> {
        private final CountDownLatch countDownLatch;

        private Boolean gotStats = false;

        public StatsCallback(CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void onNext(Statistics stats) {
            log.info("Received stats #{}: {}", countDownLatch.getCount(), stats);
            if (stats != null) {
                gotStats = true;
            }
            countDownLatch.countDown();
        }

        public Boolean gotStats() {
            return gotStats;
        }
    }
}
