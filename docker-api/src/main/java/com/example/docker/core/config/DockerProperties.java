package com.example.docker.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * docker 配置信息
 *
 * @author novel
 * @date 2021/6/23 11:04
 */
@ConfigurationProperties(prefix = "docker")
@Data
public class DockerProperties {
    /**
     * tcp://localhost:2376 or unix:///var/run/docker.sock
     */
    private String host;
    /**
     * 仓库用户名
     */
    private String registryUser;
    /**
     * 仓库密码
     */
    private String registryPass;
    /**
     * 仓库邮箱
     */
    private String registryMail;
    /**
     * 仓库地址
     */
    private String registryUrl;
}
