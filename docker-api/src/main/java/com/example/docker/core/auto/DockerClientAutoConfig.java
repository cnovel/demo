package com.example.docker.core.auto;

import com.example.docker.core.config.DockerProperties;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author novel
 * @date 2021/6/23 11:15
 */
@Configuration
@EnableConfigurationProperties(DockerProperties.class)
public class DockerClientAutoConfig {
    @Bean
    public DockerClientConfig dockerClientConfig(DockerProperties dockerProperties) {

        return DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(dockerProperties.getHost())
//                .withDockerTlsVerify(true)
//                .withDockerCertPath("/home/user/.docker")
                .withRegistryUsername(dockerProperties.getRegistryUser())
                .withRegistryPassword(dockerProperties.getRegistryPass())
                .withRegistryEmail(dockerProperties.getRegistryMail())
                .withRegistryUrl(dockerProperties.getRegistryUrl())
                .build();
    }

    @Bean
    public DockerClient dockerClient(DockerClientConfig config) {
        return DockerClientBuilder.getInstance(config).build();
    }

}
