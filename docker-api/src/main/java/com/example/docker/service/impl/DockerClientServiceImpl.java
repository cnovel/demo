package com.example.docker.service.impl;

import com.example.docker.service.DockerClientService;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.async.ResultCallbackTemplate;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Statistics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * docker 客户端服务 实现
 *
 * @author novel
 * @date 2021/6/23 11:03
 */
@Slf4j
@Service
public class DockerClientServiceImpl implements DockerClientService {
    private final DockerClient dockerClient;

    public DockerClientServiceImpl(DockerClient dockerClient) {
        this.dockerClient = dockerClient;
    }

    @Override
    public CreateContainerResponse createContainers(String containerName, String imageName) {
        throw new RuntimeException("创建容器功能暂未实现");
    }

    @Override
    public void startContainer(String containerId) {
        dockerClient.startContainerCmd(containerId).exec();
    }

    @Override
    public void stopContainer(String containerId) {
        dockerClient.stopContainerCmd(containerId).exec();
    }

    @Override
    public void removeContainer(String containerId) {
        dockerClient.removeContainerCmd(containerId).exec();
    }

    @Override
    public void statsContainer(String containerId, ResultCallbackTemplate<?, Statistics> callback) {
        dockerClient.statsCmd(containerId).exec(callback);
    }

}
