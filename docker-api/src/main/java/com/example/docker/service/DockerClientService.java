package com.example.docker.service;

import com.github.dockerjava.api.async.ResultCallbackTemplate;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Statistics;

/**
 * docker 客户端服务
 *
 * @author novel
 * @date 2021/6/23 11:02
 */
public interface DockerClientService {

    /**
     * 创建容器
     *
     * @param containerName 容器名称
     * @param imageName     镜像名称
     * @return 结果
     */
    CreateContainerResponse createContainers(String containerName, String imageName);


    /**
     * 启动容器
     *
     * @param containerId 容器id
     */
    void startContainer(String containerId);

    /**
     * 停止容器
     *
     * @param containerId 容器id
     */
    void stopContainer(String containerId);

    /**
     * 删除容器
     *
     * @param containerId 容器id
     */
    void removeContainer(String containerId);

    /**
     * 查看容器的状态
     *
     * @param containerId 容器id
     * @param callback    回调
     */
    void statsContainer(String containerId, ResultCallbackTemplate<?, Statistics> callback);

}