package com.example.guava;

import com.google.common.base.Preconditions;

/**
 * 预期值判断
 *
 * @author novel
 * @date 2020/10/30
 */
public class CheckArgument {
    public static void main(String[] args) {
        String param = "www.wdbyte.com2";
        String wdbyte = "www.wdbyte.com";
        Preconditions.checkArgument(wdbyte.equals(param), "[%s] 404 NOT FOUND", param);
        // java.lang.IllegalArgumentException: [www.wdbyte.com2] 404 NOT FOUND
    }
}
