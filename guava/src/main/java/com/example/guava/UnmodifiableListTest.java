package com.example.guava;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author novel
 * @date 2020/10/30
 */
public class UnmodifiableListTest {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        arrayList.add("b");
        List<String> jdkList = Collections.unmodifiableList(arrayList);
        ImmutableList<String> immutableList = ImmutableList.copyOf(arrayList);
        arrayList.add("ccc");
        jdkList.forEach(System.out::println);// result: a b ccc
        System.out.println("-------");
        immutableList.forEach(System.out::println);// result: a b
    }
}
