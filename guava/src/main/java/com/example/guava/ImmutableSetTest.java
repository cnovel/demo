package com.example.guava;

import com.google.common.collect.ImmutableSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 不可变的集合
 *
 * @author novel
 * @date 2020/10/30
 */
public class ImmutableSetTest {
    public static void main(String[] args) {
        // 创建方式1：of
        ImmutableSet<String> immutableSet = ImmutableSet.of("a", "b", "c");
        immutableSet.forEach(System.out::println);
        // a
        // b
        // c

        // 创建方式2：builder
        ImmutableSet<String> immutableSet2 = ImmutableSet.<String>builder()
                .add("hello")
                .add(new String("未读代码"))
                .build();
        immutableSet2.forEach(System.out::println);
        // hello
        // 未读代码

        // 创建方式3：从其他集合中拷贝创建
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("www.wdbyte.com");
        arrayList.add("https");
        ImmutableSet<String> immutableSet3 = ImmutableSet.copyOf(arrayList);
        immutableSet3.forEach(System.out::println);
        // www.wdbyte.com
        // https


        //   其实 JDK 中也提供了一个不可变集合，可以像下面这样创建。

        // JDK Collections 创建不可变 List
        List<String> list = Collections.unmodifiableList(arrayList);
        list.forEach(System.out::println);// www.wdbyte.com https
        list.add("未读代码"); // java.lang.UnsupportedOperationException
    }
}
