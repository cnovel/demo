package com.example.guava;

import com.google.common.base.Preconditions;

/**
 * 非空判断
 *
 * @author novel
 * @date 2020/10/30
 */
public class CheckNotNull {
    public static void main(String[] args) {
        String param = "未读代码";
        String name = Preconditions.checkNotNull(param);
        System.out.println(name); // 未读代码
//        String param2 = null;
//        // NullPointerException
//        String name2 = Preconditions.checkNotNull(param2);
//        System.out.println(name2);


        String param2 = null;
        String name2 = Preconditions.checkNotNull(param2, "param2 is null");
// java.lang.NullPointerException: param2 is null
    }
}
