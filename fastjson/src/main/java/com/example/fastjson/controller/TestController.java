package com.example.fastjson.controller;

import com.example.fastjson.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author lz
 * @date 2018/12/27
 */
@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping("get")
    public Person list() {
        Person person = new Person();
        person.setId(1);
        person.setName("王五");
        person.setPassword("123456");
        person.setBirth(new Date());
        return person;
    }
}
